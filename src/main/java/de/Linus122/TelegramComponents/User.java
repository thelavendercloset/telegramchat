package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;

public class User {
	@SerializedName("id")
	private long id;
	@SerializedName("is_bot")
	private boolean isBot;
	@SerializedName("first_name")
	private String firstName;
	@SerializedName("last_name")
	private String lastName;
	@SerializedName("username")
	private String username;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isBot() {
		return isBot;
	}

	public void setBot(boolean bot) {
		this.isBot = bot;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
