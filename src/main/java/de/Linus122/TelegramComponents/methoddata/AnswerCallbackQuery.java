package de.Linus122.TelegramComponents.methoddata;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AnswerCallbackQuery {
    @SerializedName("callback_query_id")
    private @NotNull String callbackQueryId;
    @SerializedName("text")
    private @Nullable String text;
    @SerializedName("show_alert")
    private @Nullable Boolean showAlert;
    @SerializedName("url")
    private @Nullable String url;
    @SerializedName("cache_time")
    private @Nullable Integer cacheTime;

    public AnswerCallbackQuery(final @NotNull String callbackQueryId) {
        this.callbackQueryId = callbackQueryId;
    }

    public @NotNull String getCallbackQueryId() {
        return callbackQueryId;
    }

    public void setCallbackQueryId(final @NotNull String callbackQueryId) {
        this.callbackQueryId = callbackQueryId;
    }

    public @Nullable String getText() {
        return text;
    }

    public void setText(final @Nullable String text) {
        this.text = text;
    }

    public @Nullable Boolean getShowAlert() {
        return showAlert;
    }

    public void setShowAlert(final @Nullable Boolean showAlert) {
        this.showAlert = showAlert;
    }

    public @Nullable String getUrl() {
        return url;
    }

    public void setUrl(final @Nullable String url) {
        this.url = url;
    }

    public @Nullable Integer getCacheTime() {
        return cacheTime;
    }

    public void setCacheTime(final @Nullable Integer cacheTime) {
        this.cacheTime = cacheTime;
    }
}
