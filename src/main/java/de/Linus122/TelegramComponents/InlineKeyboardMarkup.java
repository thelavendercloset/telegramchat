package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;

public class InlineKeyboardMarkup {
    @SerializedName("inline_keyboard")
    private @NotNull List<List<InlineKeyboardButton>> inlineKeyboard;

    public InlineKeyboardMarkup(final @NotNull List<List<InlineKeyboardButton>> inlineKeyboard) {
        this.inlineKeyboard = inlineKeyboard;
    }

    public InlineKeyboardMarkup(final @NotNull InlineKeyboardButton button) {
        this.inlineKeyboard = Collections.singletonList(Collections.singletonList(button));
    }

    public @NotNull List<List<InlineKeyboardButton>> getInlineKeyboard() {
        return inlineKeyboard;
    }

    public void setInlineKeyboard(final @NotNull List<List<InlineKeyboardButton>> inlineKeyboard) {
        this.inlineKeyboard = inlineKeyboard;
    }
}
