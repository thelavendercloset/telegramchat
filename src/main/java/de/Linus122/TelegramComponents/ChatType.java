package de.Linus122.TelegramComponents;

import com.google.gson.annotations.SerializedName;

public enum ChatType {
    @SerializedName("private")
    PRIVATE,
    @SerializedName("group")
    GROUP,
    @SerializedName("supergroup")
    SUPERGROUP,
    @SerializedName("channel")
    CHANNEL
}
