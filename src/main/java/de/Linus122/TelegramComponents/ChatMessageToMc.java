package de.Linus122.TelegramComponents;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class ChatMessageToMc extends Cancellable {
	private final @NotNull UUID senderUuid;
	private final @NotNull User senderTelegram;
	private String content;

	public ChatMessageToMc(final @NotNull UUID senderUuid, String content, final @NotNull User senderTelegram) {
		this.senderUuid = senderUuid;
		this.senderTelegram = senderTelegram;
		this.content = content;
	}

	public @NotNull UUID getSenderUuid() {
		return senderUuid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public @NotNull User getSenderTelegram() {
		return senderTelegram;
	}
}
