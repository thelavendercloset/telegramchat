package de.Linus122.TelegramChat.integrations;

import org.bukkit.Server;
import org.jetbrains.annotations.NotNull;

public class DiscordSRVIntegrationProbe {
    public static boolean isAvailable(final @NotNull Server server) {
        return server.getPluginManager().getPlugin("DiscordSRV") != null;
    }
}
