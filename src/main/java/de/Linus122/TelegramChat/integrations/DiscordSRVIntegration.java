package de.Linus122.TelegramChat.integrations;

import com.google.common.collect.ImmutableMap;
import de.Linus122.TelegramChat.*;
import de.Linus122.TelegramComponents.*;
import de.Linus122.TelegramComponents.methoddata.AnswerCallbackQuery;
import de.Linus122.TelegramComponents.methoddata.EditMessageText;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.api.ListenerPriority;
import github.scarsz.discordsrv.api.Subscribe;
import github.scarsz.discordsrv.api.events.DiscordGuildMessageReceivedEvent;
import github.scarsz.discordsrv.dependencies.jda.api.OnlineStatus;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.regex.Pattern;

public class DiscordSRVIntegration implements Integration {
    private final @NotNull Main main;
    private final @NotNull DiscordSRV discord;
    //private final @Null

    public DiscordSRVIntegration(final @NotNull Main main) {
        this.main = main;
        this.discord = DiscordSRV.getPlugin();
        //main.getConfig().getConfigurationSection()
    }

    private TextChannel getGlobalChannel() {
        return discord.getDestinationTextChannelForGameChannelName("global");
    }

    @Override
    public void start() {
        DiscordSRV.api.subscribe(this);
    }

    @Override
    public void stop() {
        DiscordSRV.api.unsubscribe(this);
    }

    @SuppressWarnings("unused")
    @Subscribe(priority = ListenerPriority.MONITOR)
    public void onMessageReceived(final @NotNull DiscordGuildMessageReceivedEvent event) {
        if (!event.getChannel().getId().equals(getGlobalChannel().getId())) {
            return;
        }
        main.getTelegram().sendAll(ChatMessageToTelegram.markdown(Utils.formatMSG("discord-to-telegram-message",
                Utils.escape(event.getAuthor().getName()), Utils.escape(event.getMessage().getContentDisplay()))[0]));
    }

    @Override
    public void onSendToTelegram(final @NotNull ChatMessageToTelegram chat) {
        // No-op.
    }

    @Override
    public void onSendToMinecraft(final @NotNull ChatMessageToMc chatMsg) {
        main.getServer().getScheduler().runTask(main, () -> {
            final OfflinePlayer op = main.getServer().getOfflinePlayer(chatMsg.getSenderUuid());
            getGlobalChannel().sendMessage(Utils.formatMSG("telegram-to-discord-message",
                    op.getName(), chatMsg.getContent())[0]).queue();
        });
    }

    @Override
    public Map<@NotNull String, BotCommandInfo> getBotCommandMap() {
        return ImmutableMap.of("discordonline",
                new BotCommandInfo(this::onDiscordOnlineCommand, "List online Discord members"));
    }

    @Override
    public Map<@NotNull Pattern, @NotNull BotCallbackFunc> getBotCallbackMap() {
        return ImmutableMap.of(Pattern.compile("discordonline"), this::onDiscordOnlineCallback);
    }

    private static final Map<@NotNull OnlineStatus, @NotNull Integer> STATUS_PRIORITIES;
    static {
        STATUS_PRIORITIES = new EnumMap<>(OnlineStatus.class);
        STATUS_PRIORITIES.put(OnlineStatus.ONLINE, 10);
        STATUS_PRIORITIES.put(OnlineStatus.IDLE, 9);
        STATUS_PRIORITIES.put(OnlineStatus.DO_NOT_DISTURB, 8);
        STATUS_PRIORITIES.put(OnlineStatus.INVISIBLE, 7);
        STATUS_PRIORITIES.put(OnlineStatus.OFFLINE, 6);
    }

    private @NotNull String buildOnlineMessage() {
        final StringBuilder sb = new StringBuilder();
        final List<Member> members = new ArrayList<>(getGlobalChannel().getMembers());
        members.sort((a, b) -> {
            int statusA = STATUS_PRIORITIES.getOrDefault(a.getOnlineStatus(), 0);
            int statusB = STATUS_PRIORITIES.getOrDefault(b.getOnlineStatus(), 0);
            if (statusA != statusB) {
                return statusA - statusB;
            }
            return a.getUser().getAsTag().compareTo(b.getUser().getAsTag());
        });
        for (final @NotNull Member member : members) {
            if (member.getUser().isBot()) {
                continue;
            }
            int statusCodepoint;
            switch (member.getOnlineStatus()) {
                case ONLINE:
                    statusCodepoint = 0x1F7E2;
                    break;
                case IDLE:
                    statusCodepoint = 0x1F7E0;
                    break;
                case DO_NOT_DISTURB:
                    statusCodepoint = 0x1F534;
                    break;
                case INVISIBLE:
                    statusCodepoint = 0x26AA;
                    break;
                case OFFLINE:
                    statusCodepoint = 0x26AB;
                    break;
                default:
                    statusCodepoint = 0x1F518;
                    break;
            }
            sb.append(Character.toChars(statusCodepoint));
            sb.append(" *");
            sb.append(Utils.escape(member.getEffectiveName()));
            sb.append("* \\(_");
            sb.append(Utils.escape(member.getUser().getAsTag()));
            sb.append("_\\) ");
            if (member.getVoiceState() != null && member.getVoiceState().inVoiceChannel()) {
                if (member.getVoiceState().isMuted()) {
                    sb.append(Character.toChars(0x1F507));  // Crossed loudspeaker
                } else {
                    sb.append(Character.toChars(0x1F50A));  // Loudspeaker
                }
            }
            sb.append('\n');
        }
        return sb.toString();
    }

    private void onDiscordOnlineCommand(final @NotNull Telegram bot, final @NotNull Message triggeringMessage,
                                        final @NotNull String args) {
        final ChatMessageToTelegram msg = ChatMessageToTelegram.markdown(buildOnlineMessage());
        msg.setChatId(triggeringMessage.getChat().getId());
        msg.setReplyMarkup(new InlineKeyboardMarkup(new InlineKeyboardButton("Refresh", "discordonline")));
        bot.sendMsg(msg);
    }

    private void onDiscordOnlineCallback(final @NotNull Telegram bot, final @NotNull CallbackQuery query) {
        final Message triggeringMsg = query.getMessage();
        if (triggeringMsg == null) {
            return;  // No.
        }
        final EditMessageText edit = EditMessageText.markdown(triggeringMsg, buildOnlineMessage());
        edit.setReplyMarkup(new InlineKeyboardMarkup(new InlineKeyboardButton("Refresh", "discordonline")));
        bot.editMessageText(edit);

        bot.answerCallbackQuery(new AnswerCallbackQuery(query.getId()));
    }
}
