package de.Linus122.TelegramChat.integrations;

import de.Linus122.TelegramChat.BotCallbackFunc;
import de.Linus122.TelegramChat.BotCommandInfo;
import de.Linus122.TelegramChat.TelegramActionListener;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

public interface Integration extends TelegramActionListener {
    void start();
    void stop();

    default Map<@NotNull String, @NotNull BotCommandInfo> getBotCommandMap() {
        return Collections.emptyMap();
    }

    default Map<@NotNull Pattern, @NotNull BotCallbackFunc> getBotCallbackMap() {
        return Collections.emptyMap();
    }
}
