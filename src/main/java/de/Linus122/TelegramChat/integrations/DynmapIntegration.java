package de.Linus122.TelegramChat.integrations;

import com.google.common.collect.ImmutableMap;
import de.Linus122.TelegramChat.*;
import de.Linus122.TelegramComponents.*;
import de.Linus122.TelegramComponents.methoddata.AnswerCallbackQuery;
import de.Linus122.TelegramComponents.methoddata.EditMessageText;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.dynmap.DynmapCommonAPI;
import org.dynmap.DynmapWebChatEvent;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

public class DynmapIntegration implements Integration, Listener {
    private final @NotNull Main main;
    private final @NotNull DynmapCommonAPI dynmap;

    public DynmapIntegration(final @NotNull Main main) {
        this.main = main;
        this.dynmap = (DynmapCommonAPI) Objects.requireNonNull(main.getServer().getPluginManager().getPlugin("dynmap"));
    }

    @Override
    public void start() {
        main.getServer().getPluginManager().registerEvents(this, main);
    }

    @Override
    public void stop() {
        // No-op.
    }

    @Override
    public void onSendToTelegram(final @NotNull ChatMessageToTelegram chat) {
        // No-op.
    }

    @Override
    public void onSendToMinecraft(final @NotNull ChatMessageToMc chatMsg) {
        main.getServer().getScheduler().runTask(main, () -> {
            final OfflinePlayer op = main.getServer().getOfflinePlayer(chatMsg.getSenderUuid());
            dynmap.sendBroadcastToWeb(Utils.formatMSG(
                    "dynmap-nickname-format", op.getName())[0], chatMsg.getContent());
        });
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDynmapWebChat(final @NotNull DynmapWebChatEvent event) {
        if (!main.getTelegram().isConnected()) {
            return;
        }
        ChatMessageToTelegram chat = new ChatMessageToTelegram();
        chat.setParseMode(ParseMode.MARKDOWNV2);
        chat.setText(Utils.formatMSG("dynmap-message-to-telegram",
                Utils.escape(event.getName()),
                Utils.escape(event.getMessage()))[0]);
        main.getTelegram().sendAll(chat);
    }

    @Override
    public Map<@NotNull String, BotCommandInfo> getBotCommandMap() {
        return ImmutableMap.of("biscordonline",
                new BotCommandInfo(this::onDiscordOnlineCommand, "List online Biscord members"));
    }

    @Override
    public Map<@NotNull Pattern, @NotNull BotCallbackFunc> getBotCallbackMap() {
        return ImmutableMap.of(Pattern.compile("biscordonline"), this::onDiscordOnlineCallback);
    }

    private @NotNull String buildOnlineMessage() {
        return Utils.escape(String.valueOf(ThreadLocalRandom.current().nextInt()));
    }

    private void onDiscordOnlineCommand(final @NotNull Telegram bot, final @NotNull Message triggeringMessage,
                                        final @NotNull String args) {
        final ChatMessageToTelegram msg = ChatMessageToTelegram.markdown(buildOnlineMessage());
        msg.setChatId(triggeringMessage.getChat().getId());
        msg.setReplyMarkup(new InlineKeyboardMarkup(new InlineKeyboardButton("Refresh", "biscordonline")));
        bot.sendMsg(msg);
    }

    private void onDiscordOnlineCallback(final @NotNull Telegram bot, final @NotNull CallbackQuery query) {
        final Message triggeringMsg = query.getMessage();
        if (triggeringMsg == null) {
            return;  // No.
        }
        final String online = buildOnlineMessage();
        if (!online.equals(triggeringMsg.getText())) {
            final EditMessageText edit = EditMessageText.markdown(triggeringMsg, online);
            edit.setReplyMarkup(new InlineKeyboardMarkup(new InlineKeyboardButton("Refresh", "biscordonline")));
            bot.editMessageText(edit);
        }
        bot.answerCallbackQuery(new AnswerCallbackQuery(query.getId()));
    }
}
