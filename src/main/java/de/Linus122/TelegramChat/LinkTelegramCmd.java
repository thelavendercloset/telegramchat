package de.Linus122.TelegramChat;

import com.google.gson.JsonObject;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class LinkTelegramCmd implements CommandExecutor {
	private final @NotNull Main main;

	public LinkTelegramCmd(final @NotNull Main main) {
		this.main = main;
	}

	@SuppressWarnings("squid:S3516")
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if (!(cs instanceof Player)) {
			cs.sendMessage(Utils.formatMSG("cant-link-console")[0]);
			return true;
		}
		if (!cs.hasPermission("telegram.linktelegram")) {
			cs.sendMessage(Utils.formatMSG("no-permissions")[0]);
			return true;
		}
		final JsonObject auth = main.getTelegram().getAuthJson();
		if (auth == null) {
			cs.sendMessage(Utils.formatMSG("need-to-add-bot-first")[0]);
			return true;
		}

		String token = Main.generateLinkToken();
		main.getData().addLinkCode(token, ((Player) cs).getUniqueId());
		cs.sendMessage(Utils.formatMSG("get-token",
				auth.getAsJsonObject("result").get("username").getAsString(),
				auth.getAsJsonObject("result").get("username").getAsString(), token));

		return true;
	}

}
