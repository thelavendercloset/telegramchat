package de.Linus122.TelegramChat;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import com.google.gson.*;

import de.Linus122.TelegramComponents.*;

import de.Linus122.TelegramComponents.methoddata.AnswerCallbackQuery;
import de.Linus122.TelegramComponents.methoddata.EditMessageText;
import org.bukkit.configuration.file.FileConfiguration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;

public class Telegram {
	private static final String API_URL_GETME = "https://api.telegram.org/bot%s/getMe";
	private static final String API_URL_GETUPDATES = "https://api.telegram.org/bot%s/getUpdates?offset=%d&timeout=%d";
	private static final String API_URL_GENERAL = "https://api.telegram.org/bot%s/%s";
	private static final int TIMEOUT = 60;
	private static final Gson GSON = new Gson();
	private static final JsonParser JSON_PARSER = new JsonParser();
	private static final JsonObject EMPTY_RESPONSE;
	static {
		EMPTY_RESPONSE = new JsonObject();
		EMPTY_RESPONSE.add("ok", new JsonPrimitive(true));
		EMPTY_RESPONSE.add("result", new JsonArray());
	}

	private final @NotNull Main main;
	private final @NotNull Logger logger;
	private final @NotNull FileConfiguration cfg;
	private final @NotNull Proxy proxy;
	private final List<@NotNull TelegramActionListener> listeners = new ArrayList<>();
	private final Map<@NotNull String, @NotNull BotCommandInfo> commands = new HashMap<>();
	private final Map<@NotNull Pattern, @NotNull BotCallbackFunc> callbacks = new HashMap<>();

	private @Nullable JsonObject authJson;
	private @Nullable Long botId;
	private boolean connected = false;

	private long lastUpdate = 0;
	private String token;

	public Telegram(final @NotNull Main main, final @NotNull Logger logger, final @NotNull FileConfiguration cfg) {
		this.main = main;
		this.logger = logger;
		this.cfg = cfg;
		this.proxy = cfg.getBoolean("enable-proxy") ?
				new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
						cfg.getString("proxy.address"), cfg.getInt("proxy.port")))
				: Proxy.NO_PROXY;
	}

	public void addListener(final @NotNull TelegramActionListener actionListener) {
		listeners.add(actionListener);
	}

	public void addCommands(Map<@NotNull String, @NotNull BotCommandInfo> commands) {
		this.commands.putAll(commands);
	}

	public void addCallbacks(Map<@NotNull Pattern, @NotNull BotCallbackFunc> callbacks) {
		this.callbacks.putAll(callbacks);
	}

	public boolean isConnected() {
		return connected;
	}

	public @Nullable JsonObject getAuthJson() {
		return authJson;
	}

	/**
	 * Sets the auth token and attemps connection to the Telegram API.
	 * @param token Bot auth token.
	 * @see #isConnected()
	 */
	public void connect(String token) {
		this.token = token;
		reconnect();
	}

	/**
	 * Attempts a reconnection to the Telegram API if the bot got disconnected.
	 * @see #isConnected()
	 */
	public void reconnect() {
		try {
			authJson = sendGet(String.format(API_URL_GETME, token));
			botId = authJson.get("result").getAsJsonObject().get("id").getAsLong();
			connected = true;
			logger.info("Connection established.");
			final List<@NotNull BotCommand> cmdEntries = new ArrayList<>(commands.size());
			for (final Map.Entry<@NotNull String, @NotNull BotCommandInfo> entry : commands.entrySet()) {
				cmdEntries.add(new BotCommand(entry.getKey(), entry.getValue().getDescription()));
			}
			setMyCommands(cmdEntries).thenRun(() -> logger.log(Level.INFO,"{0} commands set.", commands.size()));
		} catch (Exception e) {
			authJson = null;
			botId = null;
			connected = false;
			logger.log(Level.INFO, "Connection failed.", e);
		}
	}

	public void getUpdates(final @NotNull Consumer<@Nullable Interruptible> interruptDelegator) {
		final long groupid =  cfg.getLong("group-id");
		final JsonObject up;
		try {
			up = sendGet(String.format(API_URL_GETUPDATES, main.getData().getToken(), lastUpdate + 1, TIMEOUT),
					interruptDelegator);
		} catch (final HttpException e) {
			final String desc = e.data.get("description").getAsString();
			if (e.getHttpCode() == 400) {
				logger.log(Level.WARNING, e, () -> "Failed to get updates: " + e.getHttpCode() + ", " + desc);
			} else {
				logger.log(Level.WARNING, "Failed to get updates: {0}, {1}", new String[]{
						String.valueOf(e.getHttpCode()),
						desc
				});
				connected = false;
			}
			return;
		} catch (final IOException e){
			logger.log(Level.WARNING, "Failed to get updates.", e);
			connected = false;
			return;
		}
		if (!up.has("result")) {
			return;
		}
		for (JsonElement ob : up.getAsJsonArray("result")) {
			if (!ob.isJsonObject()) {
				continue;
			}
			final Update update = GSON.fromJson(ob, Update.class);

			if (update.getUpdateId() <= lastUpdate)
				continue;
			lastUpdate = update.getUpdateId();

			final Message msg = update.getMessage();
			if (msg != null) {
				final Chat chat = msg.getChat();
				if (!main.getData().ids.contains(chat.getId()) )
					main.getData().ids.add(chat.getId());

				final String text = msg.getText();
				if (text != null && !text.isEmpty()) {
					if (text.startsWith("/")) {
						switch (text) {
							case "/start":
								if (main.getData().isFirstUse()) {
									main.getData().setFirstUse(false);
									ChatMessageToTelegram chat2 = new ChatMessageToTelegram();
									chat2.setChatId(chat.getId());
									chat2.setParseMode(ParseMode.MARKDOWNV2);
									chat2.setText(Utils.formatMSG("setup-msg")[0]);
									this.sendMsg(chat2);
								}
								this.sendMsg(chat.getId(), Utils.formatMSG("can-see-but-not-chat")[0]);
								break;
							case "/getChatId":
//											this.sendMsg(chat.getId(), Integer.toString(chat.getId()));
								this.sendMsg(chat.getId(), Utils.formatMSG("get-chat-id", Long.toString(chat.getId()))[0]);
								break;
							default:
								final int idxOfSpace = text.indexOf(' ');
								final String cmdName = text.substring(1, idxOfSpace == -1 ? text.length() : idxOfSpace);
								final BotCommandInfo cmd = commands.get(cmdName);
								if (cmd != null) {
									cmd.getFunc().execute(this, msg, text);
								}
						}
					} else if (chat.isPrivate() && main.getData().getLinkCodes().containsKey(text)) {
						// LINK
						main.link(main.getData().getUUIDFromLinkCode(text), chat.getId());
						main.groupLink(main.getData().getUUIDFromLinkCode(text), msg.getFrom().getId(), chat.getId());

						main.getData().removeLinkCode(text);
					} else if (main.getData().getGroupLinkedChats().containsKey(msg.getFrom().getId()) && chat.getId() == groupid) {
						final ChatMessageToMc chatMsg = new ChatMessageToMc(
								main.getData().getUUIDFromUserID(msg.getFrom().getId()), text, msg.getFrom());
						for (final TelegramActionListener actionListener : listeners) {
							actionListener.onSendToMinecraft(chatMsg);
						}
						if (!chatMsg.isCancelled()) {
							main.sendToMC(chatMsg);
						}
					} else {
						this.sendMsg(chat.getId(), Utils.formatMSG("need-to-link")[0]);
					}
				}
			}

			final CallbackQuery callbackQuery = update.getCallbackQuery();
			if (callbackQuery != null && callbackQuery.getData() != null) {
				for (final Map.Entry<@NotNull Pattern, @NotNull BotCallbackFunc> callbackEntry : callbacks.entrySet()) {
					if (callbackEntry.getKey().matcher(callbackQuery.getData()).matches()) {
						callbackEntry.getValue().execute(this, callbackQuery);
						break;
					}
				}
			}
		}
		return;
	}

	public void sendMsg(long id, String msg) {
		ChatMessageToTelegram chat = new ChatMessageToTelegram();
		chat.setChatId(id);
		chat.setText(msg);
		sendMsg(chat);
	}

	public void sendMsg(ChatMessageToTelegram chat) {
		for (final TelegramActionListener actionListener : listeners) {
			actionListener.onSendToTelegram(chat);
		}
		if (!chat.isCancelled()) {
			try {
				post("sendMessage", GSON.toJson(chat, ChatMessageToTelegram.class));
			} catch (final HttpException e) {
				final String desc = e.data.get("description").getAsString();
				if (e.getHttpCode() == 400) {
					logger.log(Level.WARNING, e, () -> "Failed to send message \"" + chat.getText() + "\": " +
							e.getHttpCode() + ", " + desc);
				} else {
					logger.log(Level.WARNING, "Failed to send message \"{0}\": {1}, {2}", new String[]{
							chat.getText(),
							String.valueOf(e.getHttpCode()),
							desc
					});
					connected = false;
				}
			} catch (final IOException e){
				logger.log(Level.WARNING, "Failed to send message.", e);
				connected = false;
			}
		}
	}

	public void sendAll(final ChatMessageToTelegram chat) {
		chat.setChatId(cfg.getLong("group-id"));
		sendMsg(chat);
	}

	public CompletableFuture<JsonObject> answerCallbackQuery(final @NotNull AnswerCallbackQuery answer) {
		return wrapPost("answerCallbackQuery", GSON.toJson(answer));
	}

	public CompletableFuture<JsonObject> editMessageText(final @NotNull EditMessageText edit) {
		return wrapPost("editMessageText", GSON.toJson(edit));
	}

	private CompletableFuture<JsonObject> setMyCommands(final @NotNull List<@NotNull BotCommand> commands) {
		final JsonObject obj = new JsonObject();
		obj.add("commands", GSON.toJsonTree(commands));
		return wrapPost("setMyCommands", GSON.toJson(obj));
	}

	private CompletableFuture<JsonObject> wrapPost(final @NotNull String method, final @NotNull String json) {
		try {
			return CompletableFuture.completedFuture(post(method, json));
		} catch (final HttpException e) {
			final String desc = e.data.get("description").getAsString();
			if (e.getHttpCode() == 400) {
				logger.log(Level.WARNING, e, () -> method + " failed: " + e.getHttpCode() + ", " + desc);
			} else {
				logger.log(Level.WARNING, "{0} failed: {1}, {2}", new String[]{
						method,
						String.valueOf(e.getHttpCode()),
						desc
				});
			}
			final CompletableFuture<JsonObject> future = new CompletableFuture<>();
			future.completeExceptionally(e);
			return future;
		} catch (final IOException e){
			logger.log(Level.WARNING, e, () -> method + " failed");
			final CompletableFuture<JsonObject> future = new CompletableFuture<>();
			future.completeExceptionally(e);
			return future;
		}
	}

	public @NotNull JsonObject post(final @NotNull String method, final @NotNull String json)
			throws HttpException, IOException {
		logger.log(Level.FINEST, "post({0}, {1})", new String[]{method, json});
		final URL url = new URL(String.format(API_URL_GENERAL, main.getData().getToken(), method));
		final HttpsURLConnection conn = (HttpsURLConnection) url.openConnection(proxy);
		conn.setRequestMethod("POST");
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setUseCaches(false);
		conn.setReadTimeout((TIMEOUT + 1) * 1000);
		conn.setRequestProperty("Content-Type", "application/json; ; Charset=UTF-8");
		conn.setRequestProperty("Content-Length", String.valueOf(json.length()));

		final OutputStream out = conn.getOutputStream();
		out.write(json.getBytes(StandardCharsets.UTF_8));
		out.flush();
		out.close();

		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new HttpException(conn.getResponseCode(),
					JSON_PARSER.parse(new InputStreamReader(conn.getErrorStream())).getAsJsonObject());
		}

		return JSON_PARSER.parse(new InputStreamReader(conn.getInputStream())).getAsJsonObject();
	}

	public @NotNull JsonObject sendGet(final @NotNull String url,
									   final @Nullable Consumer<@Nullable Interruptible> interruptDelegator)
			throws HttpException, IOException {
		logger.log(Level.FINEST, "sendGet({0})", url);
		final HttpsURLConnection conn = (HttpsURLConnection) new URL(url).openConnection(proxy);
		conn.setUseCaches(false);
		conn.setReadTimeout((TIMEOUT + 1) * 1000);

		conn.connect();
		final Utils.Box<Boolean> interrupted = new Utils.Box<>(false);
		if (interruptDelegator != null) {
			interruptDelegator.accept(() -> {
				logger.log(Level.INFO, "Closing long-polling socket");
				interrupted.set(true);
				conn.disconnect();
			});
		}

		try {
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new HttpException(conn.getResponseCode(),
						JSON_PARSER.parse(new InputStreamReader(conn.getErrorStream())).getAsJsonObject());
			}
			final JsonObject obj = JSON_PARSER.parse(new InputStreamReader(conn.getInputStream())).getAsJsonObject();
			if (interruptDelegator != null) {
				interruptDelegator.accept(null);
			}
			return obj;
		} catch (final SocketException | SSLException e) {
			if (interrupted.get()) {
				return EMPTY_RESPONSE;
			}
			throw e;
		} finally {
			conn.disconnect();
		}
	}

	public @NotNull JsonObject sendGet(final @NotNull String url) throws HttpException, IOException {
		return sendGet(url, null);
	}

	private static class HttpException extends Exception {
		private final int httpCode;
		private final transient @NotNull JsonObject data;

		public HttpException(int httpCode, @NotNull JsonObject data) {
			super("Server returned HTTP response code: " + httpCode);
			this.httpCode = httpCode;
			this.data = data;
		}

		public int getHttpCode() {
			return httpCode;
		}

		public @NotNull JsonObject getData() {
			return data;
		}
	}
}
