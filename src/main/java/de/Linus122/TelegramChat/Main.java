package de.Linus122.TelegramChat;

import com.google.gson.Gson;
import com.vdurmont.emoji.EmojiParser;
import de.Linus122.TelegramChat.integrations.DiscordSRVIntegration;
import de.Linus122.TelegramChat.integrations.DiscordSRVIntegrationProbe;
import de.Linus122.TelegramChat.integrations.DynmapIntegration;
import de.Linus122.TelegramChat.integrations.DynmapIntegrationProbe;
import de.Linus122.TelegramChat.integrations.Integration;
import de.Linus122.TelegramComponents.ChatMessageToMc;
import de.Linus122.TelegramComponents.ChatMessageToTelegram;
import de.Linus122.TelegramComponents.ParseMode;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;

public class Main extends JavaPlugin implements Listener {
	private static Main instance;

	public static @NotNull Main getInstance() {
		return instance;
	}

	private static void setInstance(Main instance) {
		Main.instance = instance;
	}

	private static final File DATA_FILE = new File("plugins/TelegramChat/data.json");
	private FileConfiguration cfg;

	private Data data;
	private Telegram telegramHook;
	private final List<@NotNull Integration> integrations = new ArrayList<>();

	private UpdateLoop updateLoop;
	private static final long RECONNECT_PERIOD = 40L;

	@Override
	public void onEnable() {
		setInstance(this);
		super.saveDefaultConfig();

		cfg = this.getConfig();
		Utils.cfg = cfg;

		Bukkit.getPluginCommand("telegram").setExecutor(new TelegramCmd(this));
		Bukkit.getPluginCommand("linktelegram").setExecutor(new LinkTelegramCmd(this));
		Bukkit.getPluginManager().registerEvents(this, this);

		if (DATA_FILE.exists()) {
			final Gson gson = new Gson();
			try (final InputStream fin = new FileInputStream(DATA_FILE)) {
				data = gson.fromJson(new InputStreamReader(fin), Data.class);
			} catch (final IOException e) {
				getLogger().log(Level.WARNING, "Failed to read data.", e);
				data = new Data();
			}
		} else {
			data = new Data();
		}

		telegramHook = new Telegram(this, getLogger(), cfg);

		if (DynmapIntegrationProbe.isAvailable(getServer())) {
			integrations.add(new DynmapIntegration(this));
			getLogger().info("Starting Dynmap integration");
		}
		if (DiscordSRVIntegrationProbe.isAvailable(getServer())) {
			integrations.add(new DiscordSRVIntegration(this));
			getLogger().info("Starting DiscordSRV integration");
		}
		for (final @NotNull Integration integration : integrations) {
			telegramHook.addListener(integration);
			telegramHook.addCommands(integration.getBotCommandMap());
			telegramHook.addCallbacks(integration.getBotCallbackMap());
			integration.start();
		}

		telegramHook.connect(data.getToken());

		updateLoop = new UpdateLoop();
		updateLoop.runTaskLaterAsynchronously(this, 20L);

		if (telegramHook.isConnected()) {
			final ChatMessageToTelegram chat = new ChatMessageToTelegram();
			chat.setParseMode(ParseMode.MARKDOWNV2);
			chat.setText(Utils.formatMSG("start-stop", "started")[0]);
			getServer().getScheduler().runTaskAsynchronously(this, () -> telegramHook.sendAll(chat));
		}
	}

	private final class UpdateLoop extends BukkitRunnable {
		private Interruptible socketInterruptor = null;

		@Override
		public void run() {
			getLogger().log(Level.FINE, "Get updates; connected={0}", telegramHook.isConnected());
			if (!telegramHook.isConnected()) {
				getLogger().info("Disconnected from Telegram. Reconnecting.");
				telegramHook.reconnect();
				if (!telegramHook.isConnected() && !isCancelled() && Main.this.isEnabled()) {
					getLogger().log(Level.INFO,"Reconnecting failed. Trying again in {0} ticks.", RECONNECT_PERIOD);
					updateLoop = new UpdateLoop();
					updateLoop.runTaskLaterAsynchronously(Main.this, RECONNECT_PERIOD);
					return;
				}
			}
			telegramHook.getUpdates(i -> socketInterruptor = i);  // Long-polling
			if (!isCancelled() && Main.this.isEnabled()) {
				updateLoop = new UpdateLoop();
				updateLoop.runTaskLaterAsynchronously(Main.this, 1L);
			}
		}

		@Override
		public synchronized void cancel() {
			if (socketInterruptor != null) {
				socketInterruptor.interrupt();
			}
			super.cancel();
		}
	}


	@Override
	public void onDisable() {
		if (telegramHook.isConnected()) {
			final ChatMessageToTelegram chat = new ChatMessageToTelegram();
			chat.setParseMode(ParseMode.MARKDOWNV2);
			chat.setText(Utils.formatMSG("start-stop", "stopped")[0]);
			telegramHook.sendAll(chat);
		}
		for (final @NotNull Integration integration : integrations) {
			integration.stop();
		}
		updateLoop.cancel();
		save();
		setInstance(null);
	}

	public void save() {
		final Gson gson = new Gson();
		try (final OutputStream fout = new FileOutputStream(DATA_FILE)) {
			fout.write(gson.toJson(data).getBytes(StandardCharsets.UTF_8));
		} catch (final IOException e) {
			getLogger().log(Level.WARNING, "Failed to save data.", e);
		}
	}

	public @NotNull Telegram getTelegram() {
		return telegramHook;
	}

	public @NotNull Data getData() {
		return data;
	}

	public void sendToMC(ChatMessageToMc chatMsg) {
		sendToMC(chatMsg.getSenderUuid(), chatMsg.getContent());
	}

	private void sendToMC(UUID uuid, String msg) {
		final OfflinePlayer op = Bukkit.getOfflinePlayer(uuid);
		final String msgF = Utils.formatMSG("general-message-to-mc", op.getName(), msg)[0];
		Bukkit.broadcastMessage(EmojiParser.parseToAliases(msgF.replace("&", "§")));
	}

	public void link(final @NotNull UUID player, long chatID) {
		data.addChatPlayerLink(chatID, player);
		final OfflinePlayer p = Bukkit.getOfflinePlayer(player);
		telegramHook.sendMsg(chatID, Utils.formatMSG("success-linked", Utils.escapeClean(p.getName()))[0]);
	}

	public void groupLink(final @NotNull UUID player, long userID, long chatID) {
		data.addGroupChatPlayerLink(userID, player);
		final OfflinePlayer p = Bukkit.getOfflinePlayer(player);
		telegramHook.sendMsg(chatID, Utils.formatMSG("success-linked", Utils.escapeClean(p.getName()))[0]);
	}

	public static @NotNull String generateLinkToken() {
		final Random rnd = ThreadLocalRandom.current();
		final char[] chars = new char[8];
		for (int i = 0; i < 8; ++i) {
			int rndi = rnd.nextInt(2);
			if (rndi == 0) {
				chars[i] = (char)('a' + rnd.nextInt(26));
			} else {
				chars[i] = (char)('0' + rnd.nextInt(10));
			}
		}
		return new String(chars);
	}

	@EventHandler
	public void onJoin(final @NotNull PlayerJoinEvent e) {
		if (!this.getConfig().getBoolean("enable-joinquitmessages"))
			return;
		if (telegramHook.isConnected()) {
			final ChatMessageToTelegram chat = ChatMessageToTelegram.markdown(Utils.formatMSG("join-message", Utils.escapeClean(e.getPlayer().getName()))[0]);
			getServer().getScheduler().runTaskAsynchronously(this, () -> telegramHook.sendAll(chat));
		}
	}

	@EventHandler
	public void onDeath(final @NotNull PlayerDeathEvent e) {
		if (!this.getConfig().getBoolean("enable-deathmessages"))
			return;
		if (telegramHook.isConnected()) {
			final ChatMessageToTelegram chat = new ChatMessageToTelegram();
			chat.setParseMode(ParseMode.MARKDOWNV2);
			chat.setText(Utils.formatMSG("death-message", Utils.escapeClean(e.getDeathMessage()))[0]);
			getServer().getScheduler().runTaskAsynchronously(this, () -> telegramHook.sendAll(chat));
		}
	}

	@EventHandler
	public void onQuit(final @NotNull PlayerQuitEvent e) {
		if (!this.getConfig().getBoolean("enable-joinquitmessages"))
			return;
		if (telegramHook.isConnected()) {
			final ChatMessageToTelegram chat = new ChatMessageToTelegram();
			chat.setParseMode(ParseMode.MARKDOWNV2);
			chat.setText(Utils.formatMSG("quit-message", Utils.escapeClean(e.getPlayer().getName()))[0]);
			getServer().getScheduler().runTaskAsynchronously(this, () -> telegramHook.sendAll(chat));
		}
	}

	@EventHandler
	public void onChat(final @NotNull AsyncPlayerChatEvent e) {
		if (!this.getConfig().getBoolean("enable-chatmessages"))
			return;
		if (e.isCancelled())
			return;
		if (telegramHook.isConnected()) {
			final ChatMessageToTelegram chat = new ChatMessageToTelegram();
			chat.setParseMode(ParseMode.MARKDOWNV2);
			chat.setText(Utils.formatMSG("general-message-to-telegram",
					Utils.escapeClean(e.getPlayer().getDisplayName()),
					Utils.escapeClean(EmojiParser.parseToUnicode(e.getMessage())))[0]);
			getServer().getScheduler().runTaskAsynchronously(this, () -> telegramHook.sendAll(chat));
		}
	}
}
