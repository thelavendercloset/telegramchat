package de.Linus122.TelegramChat;

import de.Linus122.TelegramComponents.ChatMessageToTelegram;
import de.Linus122.TelegramComponents.ChatMessageToMc;
import org.jetbrains.annotations.NotNull;

public interface TelegramActionListener {
	void onSendToTelegram(@NotNull ChatMessageToTelegram chat);

	void onSendToMinecraft(@NotNull ChatMessageToMc chatMsg);
}
