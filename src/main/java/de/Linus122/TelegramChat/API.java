package de.Linus122.TelegramChat;

@SuppressWarnings("unused")
public class API {
	private API() {}

	public static Telegram getTelegramHook() {
		return Main.getInstance().getTelegram();
	}
}
