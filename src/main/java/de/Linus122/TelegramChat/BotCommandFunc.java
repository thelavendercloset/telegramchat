package de.Linus122.TelegramChat;

import de.Linus122.TelegramComponents.Message;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@FunctionalInterface
public interface BotCommandFunc {
    void execute(@NotNull Telegram bot, @NotNull Message triggeringMessage, @NotNull String args);
}
