package de.Linus122.TelegramChat;

import de.Linus122.TelegramComponents.CallbackQuery;
import org.jetbrains.annotations.NotNull;

@FunctionalInterface
public interface BotCallbackFunc {
    void execute(@NotNull Telegram bot, @NotNull CallbackQuery query);
}
