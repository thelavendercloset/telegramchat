package de.Linus122.TelegramChat;

import org.jetbrains.annotations.NotNull;

public class BotCommandInfo {
    private final @NotNull BotCommandFunc func;
    private final @NotNull String description;

    public BotCommandInfo(final @NotNull BotCommandFunc func, final @NotNull String description) {
        this.func = func;
        this.description = description;
    }

    public @NotNull BotCommandFunc getFunc() {
        return func;
    }

    public @NotNull String getDescription() {
        return description;
    }
}
